import React from 'react';
import LoginForm from './Login/LoginForm';
import './App.css';

class App extends React.Component {
    loginHandler(username,password) {
        alert(`User credentials. Username: ${username} Password: ${password}`);
    }
    closeHandler() {
        alert('Close the login screen');
    }
    forgotHandler() {
        alert('Forgotten password');
    }
    registerHandler() {
        alert('Registration');
    }
    render() {
        return (
            <div className="App">
                <h1>Test application</h1>
                <LoginForm
                    login={this.loginHandler}
                    close={this.closeHandler}
                    forgot={this.forgotHandler}
                    register={this.registerHandler}
                >
                </LoginForm>
            </div>
        );
    }
}

export default App;
